import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { DocumentoComponent } from './components/documento/documento.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { APP_ROUTING } from "./app.routes";
import { NosotrosComponent } from './components/nosotros/nosotros.component';
import { RedesComponent } from './components/redes/redes.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    InicioComponent,
    DocumentoComponent,
    ContactoComponent,
    NosotrosComponent,
    RedesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
