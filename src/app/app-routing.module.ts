import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactoComponent } from './components/contacto/contacto.component';
import { DocumentoComponent } from './components/documento/documento.component';
import { InicioComponent } from "./components/inicio/inicio.component";
import { NosotrosComponent } from "./components/nosotros/nosotros.component";
import { RedesComponent } from "./components/redes/redes.component";

const APP_ROUTES: Routes=[
  {path: 'inicio',component: InicioComponent},
  { path: 'documento', component: DocumentoComponent },
  { path: 'contacto', component: ContactoComponent },
  { path: 'nosotros', component: NosotrosComponent },
  { path: 'redes', component: RedesComponent },
  
  {path: '**', pathMatch: 'full', redirectTo: 'inicio'}
]


@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);